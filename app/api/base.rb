require_relative "./v1"

module API
	class Base < Grape::API
    format :json
    prefix :api

    mount V1::Base

    # before do
    #   header['Access-Control-Allow-Origin'] = '*'
    #   header['Access-Control-Request-Method'] = '*'
    # end

    add_swagger_documentation hide_documentation_path: true,
                              version: "V1",
                              info: {
                                title: 'User notes app',
                                description: 'Demo app for user notes'
                              }
  end
end