require_relative "./welcome"

module API
  module V1
    class Base < Grape::API
      version :v1, using: :path

      mount Welcome
    end
  end
end