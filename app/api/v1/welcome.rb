module API
  module V1
    class Welcome < Grape::API
      desc 'Greets user' do
        detail 'This is the root api and it will greet user on accessing'
      end
      get "/" do
        { 
          data: [
            { message: "Welcome to notes app" }
          ]
        }
      end
    end
  end
end